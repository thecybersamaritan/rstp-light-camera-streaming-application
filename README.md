# Camera Streaming Application

## Overview
This Flask-based application provides a user interface to view RTSP streams from different cameras. It allows users to add and manage camera IP addresses and ports and view live streams from added cameras.

## Prerequisites
- Docker and Docker Compose installed on your machine.
- Git installed on your machine.

## Directory Structure
```linux (Ubuntu 22.04 LTS)
.
├── Dockerfile
├── .dockerignore
├── README.md
├── app
│   ├── app.py
│   ├── config.yaml
│   ├── static
│   │   ├── css
│   │   │   └── styles.css
│   │   └── js
│   │       ├── snapshot_refresh.js
│   │       └── stream.js
│   └── templates
│       ├── index.html
│       └── stream.html
├── docker-compose.yml
└── requirements.txt
```

## Configuration

To configure the cameras you want to manage, you will need to edit the `config.yaml` file. Here's an example structure:

```
cameras:
  - ip: "176.67.105.154"
    port: "554"
```

The configuration will persist on the host, ensuring that any changes or additions to the camera list will be saved across restarts or rebuilds.

## Setup & Running

### Cloning the Repository:

```
git clone https://gitlab.com/thecybersamaritan/rstp-light-camera-streaming-application.git
cd rstp-light-camera-streaming-application
```

### Building and Running the Application:

#### Using Docker Compose:

```
docker compose up --build
```

You should now have the server running. Access the application by navigating to `https://localhost:5000` in your browser.

#### For WSL2 Users:

If you are running Docker inside a WSL2 environment, you might not be able to access the application using localhost. Instead, you need to use the WSL2 VM's IP address.

To get the IP address of the WSL2 instance, run:

```
ip addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}'
```

Use the output IP address in the browser to access the application, for example: `https://[WSL2_IP_ADDRESS]:5000`.

## Features

- View live camera snapshots.
- Add and manage cameras via the `config.yaml` file.
- View RTSP streams from individual cameras.
- Responsive design.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
