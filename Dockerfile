# Base Image
FROM python:3.9-slim-buster

# Update and install ffmpeg
RUN apt-get update && \
    apt-get install -y --no-install-recommends ffmpeg && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Set the working directory inside the container
WORKDIR /app

# Copy the dependencies file to the working directory
COPY requirements.txt requirements.txt

# Install any dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the content of the local app directory to the working directory
COPY app/ .

# Remove the copied config.yaml to ensure the volume mount will be clean
RUN rm /app/config.yaml

# Specify the command to run on container start
CMD ["python", "app.py"]