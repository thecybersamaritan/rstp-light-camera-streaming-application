document.addEventListener("DOMContentLoaded", function() {
    var video = document.getElementById('video');
    var streamUrl = '/rtsp/' + cameraId;

    function showErrorBanner(message) {
        // Add logic to show error banner with a message and timestamp
        // Example: Append a div with the message and current timestamp
        var banner = document.createElement("div");
        banner.textContent = message + " (Last checked: " + new Date().toLocaleString() + ")";
        document.body.appendChild(banner);
    }

    if (Hls.isSupported()) {
        var hls = new Hls();
        hls.loadSource(streamUrl);
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, function() {
            video.play();
        });
        hls.on(Hls.Events.ERROR, function(event, data) {
            console.error('HLS Error:', data);
            showErrorBanner("No Connection");
        });
    } else if (video.canPlayType('application/vnd.apple.mpegurl')) {
        video.src = streamUrl;
        video.addEventListener('loadedmetadata', function() {
            video.play();
        });
        video.addEventListener('error', function() {
            showErrorBanner("No Connection");
        });
    } else {
        console.error('This browser does not support MSE extensions.');
        showErrorBanner("Streaming Not Supported");
    }
});
