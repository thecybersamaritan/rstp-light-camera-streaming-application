from flask import Flask, render_template, request, redirect, url_for, abort, Response, stream_with_context, send_from_directory
import yaml
import subprocess
import logging
import os
import requests

app = Flask(__name__)

SNAPSHOT_DIR = "snapshots"
if not os.path.exists(SNAPSHOT_DIR):
    os.makedirs(SNAPSHOT_DIR)

# Logging Configuration
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = app.logger

# Load camera configs
def load_config():
    with open('config.yaml', 'r') as file:
        return yaml.safe_load(file)

config = load_config()

@app.route('/')
def index():
    cameras = config['cameras']

    for camera in cameras:
        try:
            response = requests.get(f"http://{camera['ip']}:{camera['port']}", timeout=5)
            if response.status_code == 401:  # HTTP 401 Unauthorized
                camera['auth_required'] = True
            else:
                camera['auth_required'] = False
                camera['status'] = "up"
        except requests.RequestException:
            camera['status'] = "down"

    return render_template('index.html', cameras=cameras)

@app.route('/stream/<int:camera_id>')
def stream(camera_id):
    if camera_id >= len(config['cameras']):
        abort(404)
    camera = config['cameras'][camera_id]
    return render_template('stream.html', camera=camera)

@app.route('/camera/<string:ip>/<int:port>/snapshot.jpg')
def snapshot(ip, port):
    snapshot_path = os.path.join(SNAPSHOT_DIR, f"{ip}_{port}.jpg")

    if not os.path.exists(snapshot_path):
        command = [
            "ffmpeg",
            "-analyzeduration", "100M",
            "probesize", "100M",
            "-i", f"rtsp://{ip}:{port}",
            "-frames:v", "1",
            snapshot_path
        ]
        result = subprocess.run(command, stderr=subprocess.PIPE)
        if result.returncode != 0:
            logger.error(f"Failed to capture snapshot: {result.stderr.decode('utf-8')}")
            abort(500, "Failed to capture snapshot")

    return send_from_directory(SNAPSHOT_DIR, f"{ip}_{port}.jpg")

@app.route('/rtsp/<int:camera_id>', methods=['GET'])
def rtsp_stream(camera_id):
    if camera_id >= len(config['cameras']):
        abort(404)
    camera = config['cameras'][camera_id]
    command = [
        "ffmpeg",
        "-i", f"rtsp://{camera['ip']}:{camera['port']}",
        "-c:v", "libx264",
        "-c:a", "aac",
        "-strict", "experimental",
        "-f", "hls",
        "-hls_time", "5",
        "-hls_list_size", "5",
        "-hls_flags", "delete_segments",
        "pipe:1"
    ]
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    def generate():
        while True:
            line = process.stderr.readline()
            if line:
                logging.error(f"ffmpeg error: {line.decode('utf-8').strip()}")
            output = process.stdout.read1(1024)  # read a chunk from stdout
            if not output and process.poll() is not None:
                break
            if output:
                yield output

    return Response(generate(), mimetype="application/vnd.apple.mpegurl")

@app.route('/add_camera', methods=['POST'])
def add_camera():
    ip = request.form.get('ip')
    port = request.form.get('port')

    for camera in config['cameras']:
        if camera['ip'] == ip and camera['port'] == port:
            return "Camera already exists", 400

    config['cameras'].append({'ip': ip, 'port': port})

    with open('config.yaml', 'w') as file:
        yaml.dump(config, file)

    # Reload the configuration to ensure it stays updated
    config.update(load_config())

    return redirect(url_for('index'))

@app.errorhandler(Exception)
def handle_exception(e):
    logger.error(f"Unhandled exception: {e}", exc_info=True)
    return str(e), 500

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000, ssl_context='adhoc')
